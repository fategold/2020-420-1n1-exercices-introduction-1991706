package ca.cegepdrummond;

public class Serie3_OperateursEtConditions {
    /*
     * Remplacez les ??? dans le code afin que "choix 1" soit affiché.
     */
    public void operateur1() {

        int a = 3;
        int b = 6;

       /* if (b%a ==  ) {
          System.out.println("choix 1");
        } else {
          System.out.println("choix 2");
        };*/

    }

    /*
     * Remplacez les ?? pour que "Réponse A" soit affichée
     */
    public void operateur2() {
        /* enlever cette ligne de commentaire
        int a = 3;
        int b = 6;
        int c = 2;

        if (b % a ?? c - b / a  ) {
            System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        };
        enlever cette ligne de commentaire */
    }

    /*
     * Remplacez les ?? pour que "Réponse B" soit affiché
     *
     * indice: operateur de négation.
     */
    public void operateur3() {
        /* enlever cette ligne de commentaire
        int a = 3;
        int b = 6;
        int c = 2;

        if ( ??? (b % a == c - b / a) ) {
               System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        };
        enlever cette ligne de commentaire */
    }

    /*
     * Dans l'exercise précédent, nous vous avions demandé d'utiliser une négation
     * afin d'obtenir Réponse B.
     * Cette fois-ci, changez les ?? pour un autre opérateur afin d'obtenir Réponse B.
     *
     * indice: pas égal.
     */
    public void operateur4() {
        /* enlever cette ligne de commentaire
        int a = 3;
        int b = 6;
        int c = 2;

        if ( b % a ?? c - b / a )  {
               System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        };
        enlever cette ligne de commentaire */
    }

    /*
     * Remplacer les premiers ?? pour inscrire la formule suivante: (a multiplié par b) modulo c
     * et remplacez les deuxièmes ?? par la valeur qui fera en sorte que "Réponse A" soit affiché.
     */
    public void operateur5() {
        /* enlever cette ligne de commentaire
        int a = 3;
        int b = 6;
        int c = 2;

        if ( ?? == ?? ) {
            System.out.println("Réponse A");
        } else {
            System.out.println("Réponse B");
        }
        enlever cette ligne de commentaire */
    }

    /*
     * remplacer les ?? afin de créer une fonction qui fait les opérations suivantes:
     *   -créer 3 variables de type int nommées a, b, c
     *   -mettre la valeur 4 dans a
     *   -mettre la valeur 5 dans b
     *   -mettre la somme de a et b dans c
     *   -Afficher le texte suivant:  "La somme est "  suivit de la valeur de c
     *      Ca devrait donc afficher "La somme est 9"
     */
    public void operateur6() {
        /* enlever cette ligne de commentaire

        enlever cette ligne de commentaire */
    }

    /*
     * Complétez le code pour qu'il affiche "bravo"
     */
    public void condition1() {
        /* enlever cette ligne de commentaire
        int a = 3;
        int b = 5;
        if ( ?? > ?? ) {
            System.out.println("bravo");
        }
        enlever cette ligne de commentaire */
    }

    /*
     * Corrigez le code afin d'afficher "choix 2"
     */
    public void condition2() {
        /* enlever cette ligne de commentaire
        int a = 3;
        String b = "3";
        if ( a < b ) {
            System.out.printLn("choix 1");
        } ??? {
            System.out.println("choix 2");
        }
        enlever cette ligne de commentaire */
    }

    /*
     * Complétez le code pour que "choix 3" soit affichée
     */
    public void condition3() {
        /* enlever cette ligne de commentaire
        int a = 5;
        int b = 4;
        if (a < b) {
            System.out.println("choix 1");
        } else if (a == b) {
            System.out.println("choix 2");
        } else  // <<<<< ajoutez votre code ici
        enlever cette ligne de commentaire */
    }


}
